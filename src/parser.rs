use crate::expr::{Expr, Literal};
use crate::token;
use crate::token::{Token, TokenType};
use thiserror::Error;

pub struct Parser {
    tokens: Vec<Token>,
    current: u32,
}

impl Parser {
    pub fn new(tokens: Vec<Token>) -> Self {
        Parser { tokens, current: 0 }
    }

    pub fn parse(&mut self) -> Result<Expr, ParserError> {
        self.expression()
    }

    fn expression(&mut self) -> Result<Expr, ParserError> {
        self.equality()
    }

    fn equality(&mut self) -> Result<Expr, ParserError> {
        let mut expr = self.comparison()?;

        while self.matches(&[TokenType::BangEqual, TokenType::EqualEqual]) {
            let operator = self.previous().clone();
            let right = self.comparison()?;
            expr = Expr::Binary {
                left: Box::new(expr),
                operator: operator.clone(),
                right: Box::new(right),
            };
        }

        Ok(expr)
    }

    fn comparison(&mut self) -> Result<Expr, ParserError> {
        let mut expr = self.term()?;

        while self.matches(&[
            TokenType::Greater,
            TokenType::GreaterEqual,
            TokenType::Less,
            TokenType::LessEqual,
        ]) {
            let operator = self.previous().clone();
            let right = self.term()?;
            expr = Expr::Binary {
                left: Box::new(expr),
                operator: operator.clone(),
                right: Box::new(right),
            };
        }
        Ok(expr)
    }

    fn term(&mut self) -> Result<Expr, ParserError> {
        let mut expr = self.factor()?;

        while self.matches(&[TokenType::Minus, TokenType::Plus]) {
            let operator = self.previous().clone();
            let right = self.factor()?;
            expr = Expr::Binary {
                left: Box::new(expr),
                operator: operator.clone(),
                right: Box::new(right),
            };
        }

        Ok(expr)
    }

    fn factor(&mut self) -> Result<Expr, ParserError> {
        let mut expr = self.unary()?;

        while self.matches(&[TokenType::Slash, TokenType::Star]) {
            let operator = self.previous().clone();
            let right = self.unary()?;
            expr = Expr::Binary {
                left: Box::new(expr),
                operator: operator.clone(),
                right: Box::new(right),
            };
        }

        Ok(expr)
    }

    fn unary(&mut self) -> Result<Expr, ParserError> {
        if self.matches(&[TokenType::Bang, TokenType::Minus]) {
            let operator = self.previous().clone();
            let right = self.unary()?;

            Ok(Expr::Unary {
                operator,
                right: Box::new(right),
            })
        } else {
            self.primary()
        }
    }

    fn primary(&mut self) -> Result<Expr, ParserError> {
        if self.matches(&[TokenType::False]) {
            Ok(Expr::Literal(Literal::Boolean(false)))
        } else if self.matches(&[TokenType::True]) {
            Ok(Expr::Literal(Literal::Boolean(true)))
        } else if self.matches(&[TokenType::Nil]) {
            Ok(Expr::Literal(Literal::Nil))
        } else if self.matches(&[TokenType::Number, TokenType::String]) {
            Ok(match self.previous().literal.as_ref().unwrap() {
                token::Literal::Number(value) => Expr::Literal(Literal::Number(*value)),
                token::Literal::String(value) => Expr::Literal(Literal::String(value.to_owned())),
                _ => {
                    unreachable!();
                }
            })
        } else if self.matches(&[TokenType::LeftParen]) {
            let expr = self.expression()?;
            self.consume(TokenType::RightParen, "Expect ')' after expression.")?;
            Ok(Expr::Grouping(Box::new(expr)))
        } else {
            Err(ParserError::ExpectedExpression {
                token: self.peek().clone(),
                message: "Expect expression.".to_owned(),
            })
        }
    }

    fn consume(&mut self, token_type: TokenType, message: &str) -> Result<&Token, ParserError> {
        if self.check(&token_type) {
            Ok(self.advance())
        } else {
            Err(ParserError::UnexpectedToken {
                token: self.peek().clone(),
                message: message.to_owned(),
            })
        }
    }

    fn synchronize(&mut self) {
        self.advance();

        while !self.is_at_end() {
            if let TokenType::Semicolon = self.previous().token_type {
                return;
            }

            match self.peek().token_type {
                TokenType::Class
                | TokenType::Fun
                | TokenType::Var
                | TokenType::For
                | TokenType::If
                | TokenType::While
                | TokenType::Print
                | TokenType::Return => return,
                _ => (),
            }

            self.advance();
        }
    }

    fn matches(&mut self, types: &[TokenType]) -> bool {
        if types.iter().any(|t| self.check(t)) {
            self.advance();
            return true;
        }
        false
    }

    fn previous(&self) -> &Token {
        self.tokens.get(self.current as usize - 1).unwrap()
    }

    fn check(&self, token_type: &TokenType) -> bool {
        if self.is_at_end() {
            return false;
        }

        &self.peek().token_type == token_type
    }

    fn advance(&mut self) -> &Token {
        if !self.is_at_end() {
            self.current += 1;
        }
        self.previous()
    }

    fn is_at_end(&self) -> bool {
        self.peek().token_type == TokenType::EOF
    }

    fn peek(&self) -> &Token {
        self.tokens.get(self.current as usize).unwrap()
    }
}

pub mod ast_printer {
    use crate::parser::Expr;

    pub fn print_ast(expr: &Expr) -> String {
        match expr {
            Expr::Binary {
                left,
                operator,
                right,
            } => parenthesize(&operator.lexeme, &[left, right]),
            Expr::Grouping(expression) => parenthesize("group", &[expression]),
            Expr::Literal(value) => value.to_string(),
            Expr::Unary { operator, right } => parenthesize(&operator.lexeme, &[right]),
        }
    }

    fn parenthesize(name: &str, exprs: &[&Expr]) -> String {
        let mut output = format!("({}", name);
        exprs.iter().for_each(|e| {
            output.push(' ');
            output.push_str(&print_ast(e));
        });
        output.push(')');
        output
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parser::ast_printer::print_ast;

    #[test]
    fn ast_printer_test() {
        let expr = Expr::Binary {
            left: Box::new(Expr::Unary {
                operator: Token::new(TokenType::Minus, "-".to_owned(), None, 1),
                right: Box::new(Expr::Literal(Literal::Number(123.0))),
            }),
            operator: Token::new(TokenType::Star, "*".to_owned(), None, 1),
            right: Box::new(Expr::Grouping(Box::new(Expr::Literal(Literal::Number(
                45.67,
            ))))),
        };

        let output = print_ast(&expr);
        let expected = String::from("(* (- 123) (group 45.67))");
        assert_eq!(output, expected);
    }
}

#[derive(Error, Debug)]
pub enum ParserError {
    #[error("{message}")]
    UnexpectedToken { token: Token, message: String },
    #[error("{message}")]
    ExpectedExpression { token: Token, message: String },
}
