use crate::token::{Literal, Token, TokenType};
use lazy_static::lazy_static;
use std::collections::HashMap;
use thiserror::Error;

lazy_static! {
    static ref KEYWORDS: HashMap<&'static str, TokenType> = {
        let mut keywords = HashMap::new();
        keywords.insert("and", TokenType::And);
        keywords.insert("class", TokenType::Class);
        keywords.insert("else", TokenType::Else);
        keywords.insert("false", TokenType::False);
        keywords.insert("for", TokenType::For);
        keywords.insert("fun", TokenType::Fun);
        keywords.insert("if", TokenType::If);
        keywords.insert("nil", TokenType::Nil);
        keywords.insert("or", TokenType::Or);
        keywords.insert("print", TokenType::Print);
        keywords.insert("return", TokenType::Return);
        keywords.insert("super", TokenType::Super);
        keywords.insert("this", TokenType::This);
        keywords.insert("true", TokenType::True);
        keywords.insert("var", TokenType::Var);
        keywords.insert("while", TokenType::While);
        keywords
    };
}

pub struct Scanner {
    source: Vec<char>,
    tokens: Vec<Token>,
    start: u32,
    current: u32,
    line: u32,
    pub errors: Vec<ScannerError>,
}

impl Scanner {
    pub fn new(source: &str) -> Self {
        Scanner {
            source: source.chars().collect(),
            tokens: Vec::new(),
            start: 0,
            current: 0,
            line: 1,
            errors: Vec::new(),
        }
    }

    pub fn scan_tokens(&mut self) -> Vec<Token> {
        while !self.is_at_end() {
            self.start = self.current;
            if let Err(err) = self.scan_token() {
                self.errors.push(err);
            }
        }

        self.tokens
            .push(Token::new(TokenType::EOF, "".to_string(), None, self.line));

        self.tokens.clone()
    }

    fn scan_token(&mut self) -> Result<(), ScannerError> {
        let c = self.advance();
        match c {
            '(' => self.add_token(TokenType::LeftParen, None),
            ')' => self.add_token(TokenType::RightParen, None),
            '{' => self.add_token(TokenType::LeftBrace, None),
            '}' => self.add_token(TokenType::RightBrace, None),
            ',' => self.add_token(TokenType::Comma, None),
            '.' => self.add_token(TokenType::Dot, None),
            '-' => self.add_token(TokenType::Minus, None),
            '+' => self.add_token(TokenType::Plus, None),
            ';' => self.add_token(TokenType::Semicolon, None),
            '*' => self.add_token(TokenType::Star, None),
            '!' => {
                let token = if self.matches('=') {
                    TokenType::BangEqual
                } else {
                    TokenType::Bang
                };
                self.add_token(token, None);
            }
            '=' => {
                let token = if self.matches('=') {
                    TokenType::EqualEqual
                } else {
                    TokenType::Equal
                };
                self.add_token(token, None);
            }
            '<' => {
                let token = if self.matches('=') {
                    TokenType::LessEqual
                } else {
                    TokenType::Less
                };
                self.add_token(token, None);
            }
            '>' => {
                let token = if self.matches('=') {
                    TokenType::GreaterEqual
                } else {
                    TokenType::Greater
                };
                self.add_token(token, None);
            }
            '/' => {
                if self.matches('/') {
                    while self.peek() != '\n' && !self.is_at_end() {
                        self.advance();
                    }
                } else if self.matches('*') {
                    self.block_comment()?;
                } else {
                    self.add_token(TokenType::Slash, None);
                }
            }
            ' ' | '\r' | '\t' => (),
            '\n' => self.line += 1,
            '"' => self.string()?,
            c if c.is_digit(10) => {
                self.number();
            }
            c if c.is_alphabetic() || c == '_' => {
                self.identifier();
            }
            c => {
                return Err(ScannerError::UnexpectedCharacter { c, line: self.line });
            }
        }
        Ok(())
    }

    fn block_comment(&mut self) -> Result<(), ScannerError> {
        // continue until we encounter '*/' as two next characters
        while self.peek() != '*' && self.peek_next() != '/' && !self.is_at_end() {
            if self.peek() == '\n' {
                self.line += 1;
            }
            self.advance();
        }

        // if we reached the end without encountering the enclosing '*/'
        if self.is_at_end() {
            return Err(ScannerError::UnterminatedBlockComment(self.line));
        }

        // advance past the enclosing '*/'
        self.advance();
        self.advance();

        Ok(())
    }

    fn identifier(&mut self) {
        while is_alphanumeric(self.peek()) {
            self.advance();
        }
        let text = self.source[self.start as usize..self.current as usize]
            .iter()
            .collect::<String>();
        let token_type = if KEYWORDS.contains_key(&text as &str) {
            KEYWORDS.get(&text as &str).unwrap().clone()
        } else {
            TokenType::Identifier
        };
        self.add_token(token_type, None);
    }

    fn number(&mut self) {
        while self.peek().is_digit(10) {
            self.advance();
        }

        if self.peek() == '.' && self.peek_next().is_digit(10) {
            self.advance();

            while self.peek().is_digit(10) {
                self.advance();
            }
        }

        let value: f32 = self.source[self.start as usize..self.current as usize]
            .iter()
            .collect::<String>()
            .parse()
            .unwrap();

        self.add_token(TokenType::Number, Some(Literal::Number(value)));
    }

    fn string(&mut self) -> Result<(), ScannerError> {
        while self.peek() != '"' && !self.is_at_end() {
            if self.peek() == '\n' {
                self.line += 1;
            }
            self.advance();
        }

        if self.is_at_end() {
            return Err(ScannerError::UnterminatedString(self.line));
        }

        self.advance();

        let value: String = self.source[self.start as usize + 1..self.current as usize - 1]
            .iter()
            .collect();
        self.add_token(TokenType::String, Some(Literal::String(value)));
        Ok(())
    }

    fn peek(&self) -> char {
        if self.is_at_end() {
            return '\0';
        }
        *self.source.get(self.current as usize).unwrap()
    }

    fn peek_next(&self) -> char {
        if self.current + 1 >= self.source.len() as u32 {
            return '\0';
        }
        *self.source.get(self.current as usize + 1).unwrap()
    }

    fn matches(&mut self, expected: char) -> bool {
        if self.is_at_end() {
            return false;
        }

        if *self.source.get(self.current as usize).unwrap() != expected {
            return false;
        }

        self.current += 1;
        true
    }

    fn advance(&mut self) -> char {
        let next = self.source.get(self.current as usize).unwrap();
        self.current += 1;
        *next
    }

    fn add_token(&mut self, token_type: TokenType, literal: Option<Literal>) {
        let text = &self.source[(self.start as usize)..(self.current as usize)];
        self.tokens.push(Token::new(
            token_type,
            text.iter().collect(),
            literal,
            self.line,
        ));
    }

    fn is_at_end(&self) -> bool {
        self.current >= self.source.len() as u32
    }
}

fn is_alphanumeric(c: char) -> bool {
    c.is_alphanumeric() || c == '_'
}

#[derive(Error, Debug)]
pub enum ScannerError {
    #[error("Unexpected character: {c}")]
    UnexpectedCharacter { c: char, line: u32 },
    #[error("Unterminated string")]
    UnterminatedString(u32),
    #[error("Unterminated block comment")]
    UnterminatedBlockComment(u32),
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn scans_operators() {
        let input = r#"// this is a comment
!* ==
        "#;
        let mut scanner = Scanner::new(input);
        scanner.scan_tokens();
        let expected = vec![
            Token::new(TokenType::Bang, "!".to_owned(), None, 2),
            Token::new(TokenType::Star, "*".to_owned(), None, 2),
            Token::new(TokenType::EqualEqual, "==".to_owned(), None, 2),
            Token::new(TokenType::EOF, "".to_owned(), None, 3),
        ];
        assert_eq!(scanner.tokens, expected);
    }
}
