use anyhow::Result;
use rlox::parser::ast_printer;
use rlox::parser::{Parser, ParserError};
use rlox::scanner::{Scanner, ScannerError};
use rlox::token::{Token, TokenType};
use std::env;
use std::fmt::Display;
use std::fs;
use std::io;
use std::io::Write;
use std::process;

fn main() -> Result<()> {
    let mut args = env::args();
    args.next();

    let mut lox = Lox { had_error: false };

    match args.len() {
        1 => {
            lox.run_file(&args.next().unwrap())?;
        }
        0 => {
            lox.run_prompt()?;
        }
        _ => {
            println!("Usage: rlox [script]");
            process::exit(64);
        }
    }

    Ok(())
}

struct Lox {
    had_error: bool,
}

impl Lox {
    fn run_file(&mut self, path: &str) -> Result<()> {
        let file = fs::read_to_string(path)?;
        if self.had_error {
            process::exit(65);
        }
        self.run(&file);
        Ok(())
    }

    fn run_prompt(&mut self) -> Result<()> {
        let stdin = io::stdin();
        let mut line: String = String::new();
        loop {
            print!("> ");
            io::stdout().flush().unwrap();

            stdin.read_line(&mut line)?;

            if line.trim().is_empty() {
                break;
            }

            self.run(&line);
            self.had_error = false;
            line.clear()
        }
        Ok(())
    }

    fn run(&mut self, source: &str) {
        let mut scanner = Scanner::new(source);
        let tokens = scanner.scan_tokens();

        //tokens.iter().for_each(|tok| println!("{:?}", tok));

        scanner.errors.iter().for_each(|err| match err {
            ScannerError::UnexpectedCharacter { c: _, line } => self.error(*line, &err.to_string()),
            _ => (),
        });

        let mut parser = Parser::new(tokens);
        let parsed = parser.parse();

        match parsed {
            Ok(expr) => println!("{}", ast_printer::print_ast(&expr)),
            Err(err) => match err {
                ParserError::UnexpectedToken { token, message } => {
                    self.parser_error(token, &message)
                }
                ParserError::ExpectedExpression { token, message } => {
                    self.parser_error(token, &message)
                }
            },
        }
    }

    fn error(&mut self, line: u32, message: &str) {
        self.report(line, "", message);
    }

    fn parser_error(&mut self, token: Token, message: &str) {
        match token.token_type {
            TokenType::EOF => self.report(token.line, " at end", message),
            _ => self.report(token.line, &format!("at '{}'", token.lexeme), message),
        }
    }

    fn report(&mut self, line: u32, location: &str, message: &str) {
        eprintln!("[line: {}] Error {}: {}", line, location, message);
        self.had_error = true;
    }
}
